const defaultConfig = require("@wordpress/scripts/config/webpack.config");
const path = require( 'path' );

module.exports = {
	...defaultConfig,
	entry: {
		editor: "./assets/editor.js"
	},
	output: {
		filename: '[name].js',
		path: path.resolve( __dirname, 'assets/build' )
	}
};
